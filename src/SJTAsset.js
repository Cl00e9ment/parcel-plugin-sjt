const JSONAsset = require('parcel-bundler/src/assets/JSONAsset')
const { parse } = require('simple-json-templater')

class SJTAsset extends JSONAsset {
	parse(code) {
		return parse(JSON.parse(code), process.env);
	}
}

module.exports = SJTAsset;
