const fs = require('fs');

module.exports = (bundler) => {
	bundler.addAssetType('sjt', require.resolve('./SJTAsset.js'));

	// update entry files atime and mtime to avoid caching because build depend of environment variables
	const now = Date.now();
	for (const file of bundler.entryFiles) {
		try {
			fs.utimesSync(file, now, now);
		} catch (err) {
			fs.closeSync(fs.openSync(file, 'w'));
		}
	}
};
