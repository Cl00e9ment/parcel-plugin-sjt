const chai = require('chai');
const chaiMembersDeep = require('chai-members-deep');
const path = require('path');
const Bundler = require('parcel-bundler');
const parcelPluginSJT = require('../src/index');

chai.use(chaiMembersDeep);
const expect = chai.expect;

async function bundle(entryFile) {
	const bundler = new Bundler(entryFile, {
		outDir: path.resolve(__dirname, 'dist'),
		cache: false,
		watch: false,
		sourceMaps: false,
		logLevel: 0,
	});
	parcelPluginSJT(bundler);
	return await bundler.bundle();
}

describe('Test of Parcel Plugin SJT', () => {
	it('should process .sjt file', (done) => {
		process.env.var1 = 'keepSecond'
		process.env.var2 = '123';
		bundle(path.resolve(__dirname, 'use-case/demo.sjt')).then(bundle => {
			expect(require(bundle.name)).to.identically.deep.equal({
				abc: [ 'second' ],
				def: '123',
			});
			done();
		}).catch(done);
	});
});
