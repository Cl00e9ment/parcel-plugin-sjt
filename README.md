# Parcel Plugin SJT

## Description

What this plugin do:
1. transform `.sjt` template files to JSON object using environment variables
2. output a `.js` file that exports the previous JSON object

More about SJT files [here](https://www.npmjs.com/package/simple-json-templater).

**Example:**

`src/demo.sjt` input file :

```json
{
    "abc": [
        {
            "$keepIf": "var1 is keepFirst",
            "$replaceByValue": "first"
        },
        {
            "$dropIf": "var1 is keepFirst",
            "$replaceByValue": "second"
        }
    ],
    "def": {
        "$replaceByVariable": "var2"
    }
}
```

build command:

```bash
var1=keepSecond var2=123 parcel build src/demo.sjt
```

outputing the result:

```js
console.log(require('./dist/demo.js'))
```

```json
{
    "abc": [
        "second"
    ],
    "def": "123"
}
```

## About

author: Clément Saccoccio  
contact: [clem.saccoccio@protonmail.com](mailto:clem.saccoccio@protonmail.com)  
licence: [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.txt)

![licence](https://www.gnu.org/graphics/gplv3-with-text-84x42.png)
